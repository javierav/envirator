# Envirator

ENV management for Ruby and Rails apps.


## Status

> **This project is still experimental, use with caution!**


## Installation

```
$ gem install envirator
```

`TODO`: Write an advanced installation guide


## Usage

`TODO`: Write usage instructions here


## Testing

`TODO`: Write how to test


## Contributing

Contributions are welcome, please follow [GitHub Flow](https://guides.github.com/introduction/flow/index.html)


## Versioning

**envirator** uses [Semantic Versioning 2.0.0](http://semver.org)


## License

Copyright © 2018 Javier Aranda. Released under [MIT](LICENSE.md) license.
