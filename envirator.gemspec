require_relative 'lib/envirator/version'
require 'date'

Gem::Specification.new do |spec|
  #
  ## INFORMATION
  #
  spec.name = 'envirator'
  spec.version = Envirator.version
  spec.summary = 'ENV management for Ruby and Rails apps'
  spec.description = nil
  spec.authors = ['Javier Aranda']
  spec.email = ['javier.aranda.varo@gmail.com']
  spec.license = 'MIT'
  spec.date = Date.today.strftime('%Y-%m-%d')
  spec.homepage = 'https://gitlab.com/javierav/envirator'

  #
  ## GEM
  #
  spec.bindir = 'bin'
  spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = %w[lib]
  spec.files = `git ls-files -z -- lib bin LICENSE README.md envirator.gemspec`.split("\x0")
  spec.extra_rdoc_files = %w[README.md LICENSE]
  spec.required_ruby_version = '>= 2.1'

  #
  ## DEVELOPMENT DEPENDENCIES
  #
  spec.add_development_dependency 'rake', '~> 12.3.1'
  spec.add_development_dependency 'rspec', '~> 3.7.0'
  spec.add_development_dependency 'simplecov', '~> 0.16.1'
end
